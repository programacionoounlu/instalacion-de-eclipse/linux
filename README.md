# Proceso de instalación de Eclipse en Linux

Esta guía documenta el proceso de instalación de Eclipse en Linux.

## Requisitos previos

Para poder instalar Eclipse, requerimos del JDK (Java Development Kit). Este contiene todas las herramientas y librerías necesarias para desarrollar en Java, además del entorno de ejecución JRE (Java Runtime Enviroment). **Cualquier versión mayor o igual a la 8 es útil para este curso.**

### Instalación del JDK

Utilizaremo la versión libre de la platafora de desarrollo de Java llamada OpenJDK. Es muy probable que la misma se encuentre instalada en tu distribución de Linux. Puedes comprobarlo, abriendo una terminal y ejecutando:

```bash
javac --version
```

Dicho comando debería imprimir la versión instalada del compilador de Java. En caso que el ejecutable no exista, debes instalar los paquetes correspondientes a OpenJDK. 

Si tienes Ubuntu o una distribución basada en Debian, puedes hacerlo ejecutando:

```bash
sudo apt install default-jdk
```

Si tienes una distribución que utiliza paquetes RPM, puede hacerlo ejecutando:

```bash
su -c "yum install java-latest-openjdk-devel.x86_64"
```

> El nombre de los paquetes puede variar ligeramente entre diferentes distribuciones, versiones de las mismas y versiones disponibles de OpenJDk

## Instalación de Eclipse

Para la instalación de Eclipse, debemos tener en cuenta las siguientes cuestiones:

- Puede ser descargado como un archivo comprimido o puede ser instalado mediante un instalador. Nosotros tomaremos la primer alternativa.
- Existen varias distribuciones de Eclipse. Nosotros emplearemos la destinada a desarrolladores Java.
- Las versiones más recientes de Eclipse requieren un sistema operativo de 64 bits.

Para instalarlo, simplemente descarga [este archivo](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-06/R/eclipse-java-2020-06-R-linux-gtk-x86_64.tar.gz) y descomprímelo donde desees instalarlo. Si posee una versión de 32 bits de Linux, puedes utilizar este [enlace](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-09/R/eclipse-jee-2018-09-linux-gtk.tar.gz).